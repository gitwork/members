"""Поиск проектов с форками на gitlab.com"""
import click

from git_repo_size import authorization , get_usernames_list

PER_PAGE = 100  # количество проверяемых пользователей на одной странице gitlab


class SearchForks:
    """Класс для поиска форков в проектах gitlab"""
    connect = str()
    list_fork_projects = list()
    user_list = list()

    def __init__(self, path, token):
        self.path = path
        self.token = token

    def select_fork_projects(self, user):
        """Отбор ссылок на форкнутые проекты"""
        for project in user.projects.list():
            if project.forks_count:
                self.list_fork_projects.append(project.web_url)

    def search_forks_function(self):
        """Функция, выводящая список форкнутых проектов
            у пользователей из списка, либо у всех видимых на ресурсе gitlab"""
        self.connect = authorization(self.token)

        if self.path:
            self.user_list = get_usernames_list(self.path)

        if self.user_list:
            for username in self.user_list:
                user = self.connect.users.list(username=username)[0]
                self.select_fork_projects(user)
        else:
            page = 1
            while True:
                users = self.connect.users.list(per_page=PER_PAGE, page=page)
                for user in users:
                    self.select_fork_projects(user)
                if len(users) < PER_PAGE:
                    break
                page += 1

    def fork_projects_print(self):
        """Вывод форкнутых проектов на экран"""
        print("Проекты с форками:")
        for url in self.list_fork_projects:
            print(url)


@click.command()
@click.option('-p', '--path', type=str, default="")
@click.option('-t', '--token', type=str, required=True)
def search_fork_projects(path, token):
    """Программа, выводящая список форкнутых проектов
        у пользователей из списка, либо у всех видимых на ресурсе gitlab

    Args:
        path: Путь до файла с данными пользователей
        token: Приватный токен администратора

    """
    search_object = SearchForks(path, token)
    search_object.search_forks_function()
    search_object.fork_projects_print()


# pylint: disable=no-value-for-parameter)
if __name__ == '__main__':
    search_fork_projects()
