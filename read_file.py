# pylint: disable=missing-docstring
import sys


def get_usernames_list(path):
    """ Функция, читающая из файла список usernames.
    Шаблон хранимого в файле построчно списка - fio;name;username

    Args:
        path: путь до файла со свписком пользователей
    Returns:
        usernames: Список usernames
    Raises:
        UnicodeDecodeError: Файл другого формата
        FileNotFoundError: Не получилось найти нужный файл в системе
    """

    try:
        with open(path, encoding='utf-8', mode="r") as file:
            usernames = [line.split(';')[2].rstrip() for line in file]
    except UnicodeDecodeError as err:
        sys.exit(f'Недопустимый формат входного файла. {err}')
    except FileNotFoundError as err:
        sys.exit(f'Входной файл не найден в системе. {err}')
    return usernames
