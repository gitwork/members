# Docker
**Построение docker образа**

```bash
docker build -t git-repo:1.0 -f Dockerfile .
```
# Программа git_repo_size


Программа, выводящая список больших проектов пользователей gitlab,
данные по которым находятся в заданном файле
по шаблону fio;name;username
    

**Запуск docker контейнера**

```bash
docker run --rm -p 8080:8080 -v /path/to/data:/data git-repo:1.0 ../git_repo_size.py -t <token> -p slush_list
```

_**Обязательные опции:**_

-t(--token) - токен доступа

-p(--path) - путь до файла со списком пользователей 


_**Возможные опции:**_

-i(--ignore) <путь> - игнорировать список проектов:

```bash
docker run --rm -p 8080:8080 -v /path/to/data:/data git-repo:1.0 ../git_repo_size.py -t <token> -p slush_list -i ignored_list
```

-n(--numbers) <целое число> - число выводимых в топ-списке проектов:

```bash
docker run --rm -p 8080:8080 -v /path/to/data:/data git-repo:1.0 ../git_repo_size.py -t <token> -p slush_list -n 2
```

--max <число> - значение размера(в мегабайтах), при котором выводится список проектов, превышающих его:

```bash
docker run --rm -p 8080:8080 -v /path/to/data:/data git-repo:1.0 ../git_repo_size.py -t <token> -p slush_list --max 1.1
```

--msg <путь> - сообщение в формате заголовок;описание, отправляемое в issue найденным проектам:

```bash
docker run --rm -p 8080:8080 -v /path/to/data:/data git-repo:1.0 ../git_repo_size.py -t <token> -p slush_list --msg message
```

**Запуск pylint**

```bash
pylint git_repo_size.py read_file.py
```

**Запуск тестов** 

Юниттесты:
```bash
python -m unittest tests/test_repo_size.py
```

Покрытие:
```bash
nosetests --with-coverage --cover-package=git_repo_size,read_file
```

# Программа git_repo_activity

Программа, выводящая график активности списка пользователей
за определённый промежуток времени(семестр, месяц, неделя, день) по указанной дисциплине

**Запуск docker контейнера**

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../git_repo_activity.py -t <token> -p slush_list -s test_members -f mon
```

_**Обязательные опции:**_

-t(--token)  - токен доступа

-p(--path) - путь до файла со списком пользователей 

-s(--subject) <дисциплина> - анализируемая дисциплина

-f(--frame) <временные рамки> - временные рамки для оценки активности (семестр(_'sem'_),  месяц(_'mon'_),  неделя(_'week'_),  день(_'day'_))

_**Возможные опции:**_

-o(--order) <сортировочный порядок> - порядок сортировки данных на графике (по имени пользователя(_'name'_), по активности(_'active'_)). По умолчанию флаг равен 'active':

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../git_repo_activity.py -t <token> -p slush_list -s test_members -f mon -o 'name'
```

**Запуск pylint**

```bash
pylint git_repo_activity.py
```

**Запуск тестов** 

Юниттесты:
```bash
python -m unittest tests/test_repo_activity.py
```

Покрытие:
```bash
nosetests --with-coverage --cover-package=git_repo_activity
```

# Программа git_repo_roles

Программа, выводящая список проектов пользователей gitwork, в которых содержатся лишние members. 
При указаннии соответствующих флагов: 
1. добавляет заданным проектам определённый в файле список пользователей.
1. удаляет для заданных проектов определённый в файле список пользователей.

**Запуск docker контейнера**

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../git_repo_roles.py -t <token> -p slush_list -m members.txt
```

_**Обязательные опции:**_

-t(--token)  - токен доступа

-p(--path) - путь до файла со списком пользователей 

-m(--members) - путь до файла со списком разрешённых преподавателей

_**Возможные опции:**_

-a(--add) <путь> <уровень доступа> - добавление списка members найденным проектам:

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../git_repo_roles.py -t <token> -p slush_list -m members.txt -a add.txt 10
```

-d(--delete) <путь> - удаление из списка members найденных проектов:

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../git_repo_roles.py -t <token> -p slush_list -m members.txt -d delete.txt
```

-s(--subject) <название дисциплины> - поиск проектов по определённой дисциплине:

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../git_repo_roles.py -t <token> -p slush_list -m members.txt -s timp
```

**Запуск pylint**

```bash
pylint git_repo_roles.py
```

# Программа search_forks

Скрипт поиска проектов с форками на gitlab

**Запуск docker контейнера**

```bash
docker run --rm -p 8080:8080 -v </path/to/data>:/data git-repo:1.0 ../search_forks.py -t <token> -p slush_list
```

_**Обязательные опции:**_

-t(--token)  - токен доступа

_**Возможные опции:**_

-p(--path) - путь до файла со списком пользователей. По умолчанию - отсутствует (поиск осуществляется по всем доступным пользователям gitlab)

**Запуск pylint**

```bash
pylint search_forks.py
```

# Программа git_repo_reload

Скрипт перезагрузки проекта на gitlab с удалением истории коммитов

**Запуск docker контейнера**

```bash
docker run --rm -p 8080:8080 git-repo:1.0 ../git_repo_reload.py -t LZ5jqkya5WGcBnztMW5R -i 29355397 -u banger13 -m mirgorod.andrey@yandex.ru
```

_**Обязательные опции:**_

-t(--token)  - токен доступа

-i(--id) - id перезагружаемого проект

_**Опции для корректной работы docker:**_

Данные для идентификации пользователя в docker контейнере

-u(--username) - имя пользователя на gitlab

-m(--mail) - почта указанная на gitlab

**Запуск pylint**

```bash
pylint git_repo_reload.py
