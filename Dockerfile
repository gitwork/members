FROM python:3
RUN apt-get update
RUN apt-get install git
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
RUN mkdir data
WORKDIR data
COPY git_repo_size.py /
COPY git_repo_roles.py /
COPY git_repo_reload.py /
COPY git_repo_activity.py /
COPY git_repo_forks.py /
COPY read_file.py /
ENTRYPOINT ["python3"]