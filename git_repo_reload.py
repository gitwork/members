"""Скрипт перезагрузки проекта с удалением истории коммитов"""
import sys
import os
import shutil
from time import sleep

import click
import gitlab

from git_repo_size import authorization, GIT_URL


GIT = "gitlab.com" # название сайта


class GitProcessException(Exception):
    """Класс исключений для обработки ошибок работы с gitlab"""
    def __init__(self, text):
        self.text = text

    def __str__(self):
        return repr(self.text)


# pylint: disable=too-many-instance-attributes
class GitRepoReload:
    """Класс перезалива проектов git"""
    connect = gitlab.Gitlab(GIT_URL)
    project = None
    name = str()
    new_project = dict()

    def __init__(self, _id, token="", username="", mail=""):
        self.username = username
        self.mail = mail
        self.token = token
        self._id = _id

    def rename(self):
        """Метод, переименовывающий проект"""
        self.project.name = f'ishodniy_{self.name}'
        self.project.path = f'ishodniy_{self.name}'
        self.project.save()

    def reloading_members(self):
        """Метод перегрузки members из старого проекта в новый"""
        for member in self.project.members.list():
            try:
                self.new_project.members.create({'user_id': member.id,
                                                 'access_level': member.access_level})
            except gitlab.exceptions.GitlabCreateError as err:
                if "Member already exists" in str(err):
                    pass
                else:
                    sys.exit(f'gitlab.exceptions.GitlabCreateError: {err}')

    @staticmethod
    def reloading_issues(old_project, new_project):
        """Метод перегрузки issues из старого проекта в новый"""
        for issue in old_project.issues.list():
            issue.move(new_project.id)

    def cloning_repo(self):
        """Метод клонирует старый проект, удаляя историю коммитов .git,
        инициализирует новой папкой .git, заливает проект на сайт"""
        link = self.project.web_url.replace(f"{GIT}",
                                            f"access token:{self.token}@{GIT}")

        #  идентификация, необходимая для корректной работы в докере
        if self.mail and self.username:
            os.system(f"git config --global user.email '{self.mail}'")
            os.system(f"git config --global user.name '{self.username}'")

        if os.system(f"git clone '{link}' new_project"):
            raise GitProcessException("Ошибка функции clone")
        shutil.rmtree("new_project/.git")
        os.chdir("new_project")
        os.system("git init")
        os.system("git add .")
        os.system("git commit -m 'Initial'")
        link = self.new_project.web_url.replace(f"{GIT}",
                                                f"access token:{self.token}@{GIT}")
        sleep(3)
        if os.system(f"git push --set-upstream '{link}' master"):
            raise GitProcessException("Ошибка функции push")
        os.chdir('..')

    #  pylint: disable=broad-except
    def git_repo_reload(self):
        """Метод перезаливает проект на сайт, удаляя историю коммитов"""
        self.connect = authorization(self.token)
        self.project = self.connect.projects.get(self._id)
        self.name = self.project.name
        try:
            self.rename()
        except Exception as err:
            self.rolling_back_changes(stage=1, err=err)
        try:
            self.new_project = self.connect.projects.create({'name': self.name})
            self.reloading_members()
        except Exception as err:
            self.rolling_back_changes(stage=2, err=err)
        try:
            self.reloading_issues(self.project, self.new_project)
        except Exception as err:
            self.rolling_back_changes(stage=3, err=err)
        try:
            self.cloning_repo()
        except Exception as err:
            self.rolling_back_changes(stage=4, err=err)
        print("Удаление старого...")
        self.project.delete()
        shutil.rmtree("new_project")

    def rolling_back_changes(self, stage, err):
        """Функция отката изменений(работает до момента удаления исходного проекта)

        Args:
            stage: стадия перезагрузки проекта
            err: ошибка, которая вызвала функцию
        """
        print("\nОткат изменений...")
        if stage == 4:
            shutil.rmtree(f"{os.environ.get('PWD')}/new_project", ignore_errors=True)
        if stage in [3, 4]:
            self.reloading_issues(self.new_project, self.project)
        if stage in [2, 3, 4]:
            self.new_project.delete()
        if stage in [1, 2, 3, 4]:
            print("Переименовываем...")
            sleep(5)
            self.project.name = self.name
            self.project.path = self.name
            self.project.save()
        sys.exit(f"{err}: Процесс перезагрузки остановлен")


@click.command()
@click.option('-i', '--ident', type=int, required=True)
@click.option('-t', '--token', type=str, required=True)
@click.option('-u', '--username', type=str, default="")
@click.option('-m', '--mail', type=str, default="")
def repo_reload(token, ident, username, mail):
    """Функция валидирует аргументы, передаёт их в объект
    GitRepoReload, запускает git_repo_reload

    Args:
        token: приватный токен администратора
        ident: id проекта
        username: имя пользователя на gitlab
        mail:
    """
    repo_reload_obj = GitRepoReload(ident, token, username, mail)
    repo_reload_obj.git_repo_reload()


# pylint: disable=no-value-for-parameter
if __name__ == '__main__':
    repo_reload()
