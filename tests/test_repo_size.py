"""Тестирование программы repo_size"""
import unittest
from git_repo_size import GitRepoSize, get_usernames_list

UNIT_MB = 1024 ** 2  # 1 мегабайт

# Корректные данные
TOKEN = "5qGcD6Psw88K6x-wWhsD"
PATH = "tests/test_data/slush_list"
NUMBERS = 2
MAX_ = 1.1
IGNORE = "tests/test_data/ignored_list"
MSG = "tests/test_data/message"

# Некорректные данные
FALSE_PATH = "/false/path"


class TestTopTable(unittest.TestCase):
    def setUp(self):
        self.git_repo = GitRepoSize(PATH)

    def test_table_sort(self):
        test_list = [[1579, "ссылка1"],
                     [1058576, "ссылка2"],
                     [512, "ссылка3"],
                     [12345678910,"ссылка4"]]
        true_list = [["11773.757MB","ссылка4"],
                     ["1.010MB", "ссылка2"],
                     ["1.542KB", "ссылка1"],
                     ["512B", "ссылка3"]]
        self.git_repo.top_table = test_list
        self.git_repo.top_table_sort()
        self.assertEqual(self.git_repo.top_table, true_list)


class TestReadFunction(unittest.TestCase):
    def setUp(self):
        self.git_repo = GitRepoSize(PATH, ignore=IGNORE,msg=MSG)

    def test_ignore_func(self):
        self.git_repo.ignore_list_function()
        true_set = {"https://gitwork.ru/Yaroslav_Vlasov/timp",
                    "https://gitwork.ru/проект2",
                    "https://gitwork.ru/проект3"}
        self.assertEqual(self.git_repo.ignored_projects, true_set)

    def test_issue_message_func(self):
        self.git_repo.issue_message_function()
        true_list = ["Устранение неполадки",
                     "В вашем репозитории содержатся лишние файлы, "
                     "их необходимо удалить."]
        self.assertEqual(self.git_repo.message, true_list)

    def test_read_slush_func(self):
        true_list = ["banger", "Yaroslav_Vlasov", "user1",
                     "user10", "1234", "@@@@"]
        self.assertEqual(get_usernames_list(PATH), true_list)


class TestExceptionReadFunction(unittest.TestCase):
    def setUp(self):
        self.git_repo = GitRepoSize(PATH)

    def test_false_slush_list(self):
        self.assertRaises((FileNotFoundError, SystemExit),
                          get_usernames_list, FALSE_PATH)

    def test_false_ignore_list(self):
        self.git_repo.ignore = FALSE_PATH
        self.assertRaises((FileNotFoundError, SystemExit),
                          self.git_repo.ignore_list_function)

    def test_false_issue_message(self):
        self.git_repo.msg = FALSE_PATH
        self.assertRaises((FileNotFoundError, SystemExit),
                          self.git_repo.issue_message_function)

    def test_invalid_file_coding(self):
        invalid_coding = "tests/test_data/invalid_coding"
        self.git_repo.ignore = invalid_coding
        self.assertRaises((UnicodeDecodeError, SystemExit),
                          self.git_repo.ignore_list_function)

        self.git_repo.msg = invalid_coding
        self.assertRaises((UnicodeDecodeError, SystemExit),
                          self.git_repo.issue_message_function)

        self.assertRaises((UnicodeDecodeError, SystemExit),
                          get_usernames_list, invalid_coding)


def test_select_repo(list_, ignored_proj=[], max_=0):
    top_table = list()
    for project in list_:
        size = project[0]
        project_ref = project[1]
        if project_ref in ignored_proj:
            continue
        if size >= abs(max_) * UNIT_MB:
            top_table.append([size, project_ref])
    return top_table


class TestSelectRepo(unittest.TestCase):
    def setUp(self):
        self.test_list = [[55322345, "ссылка1"],
                         [42342121, "ссылка2"],
                         [444331, "ссылка3"],
                         [0, "ссылка4"]]

    def test_select_repo_max(self):
        true_list = [[55322345, "ссылка1"],
                     [42342121, "ссылка2"],
                     [444331, "ссылка3"],
                     [0, "ссылка4"]]
        result_list = test_select_repo(self.test_list)
        self.assertEqual(result_list, true_list)

        result_list = test_select_repo(self.test_list, max_=60)
        self.assertEqual(result_list, [])

        result_list = test_select_repo(self.test_list, max_=50)
        self.assertEqual(result_list, [[55322345, "ссылка1"]])

        result_list = test_select_repo(self.test_list, max_=40)
        self.assertEqual(result_list, [[55322345, "ссылка1"],
                                        [42342121, "ссылка2"]])

        result_list = test_select_repo(self.test_list, max_=-40)
        self.assertEqual(result_list, [[55322345, "ссылка1"],
                                       [42342121, "ссылка2"]])

    def test_select_repo_ignored(self):
        ignored_list = ["ссылка1", "ссылка4"]
        result_list = test_select_repo(self.test_list, ignored_proj=ignored_list)
        self.assertEqual(result_list, [[42342121, "ссылка2"],
                                        [444331, "ссылка3"]])

        ignored_list = ["ссылка1", "ссылка2", "ссылка3", "ссылка4"]
        result_list = test_select_repo(self.test_list, ignored_proj=ignored_list)
        self.assertEqual(result_list, [])


if __name__ == '__main__':
    unittest.main()
