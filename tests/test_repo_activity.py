"""Тестирование программы repo_activity"""
import unittest
from time import sleep
from datetime import datetime
from unittest.mock import patch

from git_repo_activity import GitRepoActivity


class TestTimeFrame(unittest.TestCase):
    """Тестирование вычисления временных рамок"""
    def setUp(self):
        data = "31-08-2021"
        self.test_data = datetime.strptime(data, '%d-%m-%Y')
        self.test_obj = GitRepoActivity()

    @patch('git_repo_activity.date')
    def test_time_frame_day(self, mock_date):
        """Тестирование определения временных рамок(день)"""
        mock_date.today.return_value = self.test_data
        self.test_obj.frame = 'day'
        self.test_obj.time_frame_function()
        true_list = ["30-08-2021", "01-09-2021"]
        self.assertEqual(self.test_obj.time_frame, true_list)

    @patch('git_repo_activity.date')
    def test_time_frame_week(self, mock_date):
        """Тестирование определения временных рамок(неделя)"""
        mock_date.today.return_value = self.test_data
        self.test_obj.frame = 'week'
        self.test_obj.time_frame.clear()
        self.test_obj.time_frame_function()
        true_list = ["24-08-2021", "01-09-2021"]
        self.assertEqual(self.test_obj.time_frame, true_list)
        self.test_obj.time_frame.clear()

    @patch('git_repo_activity.date')
    def test_time_frame_mon(self, mock_date):
        """Тестирование определения временных рамок(месяц)"""
        mock_date.today.return_value = self.test_data
        self.test_obj.frame = 'mon'
        self.test_obj.time_frame.clear()
        self.test_obj.time_frame_function()
        true_list = ["01-08-2021", "01-09-2021"]
        self.assertEqual(self.test_obj.time_frame, true_list)

    @patch('git_repo_activity.date')
    def test_time_frame_sem(self, mock_date):
        """Тестирование определения временных рамок(семестр)"""

        #  определение временных рамок по дате из весеннего семестра
        mock_date.today.return_value = self.test_data
        self.test_obj.frame = 'sem'
        self.test_obj.time_frame.clear()
        self.test_obj.time_frame_function()
        true_list = ["01-01-2021", "01-09-2021"]
        self.assertEqual(self.test_obj.time_frame, true_list)

        #  определение временных рамок по дате из осеннего семестра
        mock_date.today.return_value = datetime.strptime("01-09-2021", '%d-%m-%Y')
        self.test_obj.time_frame.clear()
        self.test_obj.time_frame_function()
        true_list = ["01-09-2021", "02-09-2021"]
        self.assertEqual(self.test_obj.time_frame, true_list)


class TestWorkingData(unittest.TestCase):
    """Тестирование работы с данными"""
    def setUp(self):
        self.test_obj = GitRepoActivity()
        self.test_obj.subject = 'timp'
        self.test_obj.time_frame = ["01-01-2021", "01-09-2021"]

    def test_sort(self):
        """Тестирование сортировки данных для графического вывода"""
        self.test_obj.graphic_data = [['g', 4], ['e', 6], ['c', 8], ['a', 10], ['b', 9],
                                      ['d', 7], ['f', 5], ['h', 3], ['k', 2], ['l', 1]]
        true_list = [['l', 1], ['k', 2], ['h', 3], ['g', 4], ['f', 5],
                     ['e', 6], ['d', 7], ['c', 8], ['b', 9], ['a', 10]]
        self.test_obj.order = 'activ'  # ключ: сортировка по активности
        self.test_obj.sort_data()
        self.assertEqual(self.test_obj.graphic_data, true_list)

        self.test_obj.order = 'name'  # ключ: сортировка по имени
        self.test_obj.sort_data()
        true_list = [['a', 10], ['b', 9], ['c', 8], ['d', 7], ['e', 6],
                     ['f', 5], ['g', 4], ['h', 3], ['k', 2], ['l', 1]]
        self.assertEqual(self.test_obj.graphic_data, true_list)

    def test_graph(self):
        """Тестирование функции построения графика"""
        self.test_obj.graphic_data = [['Vitaliy Lomachenko', 1], ['Vladimir Alexandrov', 8],
                                      ['Alexey Ivanov', 15], ['Ekaterina Kupcova', 70],
                                      ['Yana Bonina', 21], ['Vitaliy Antonov', 32],
                                      ['Misha Ivanov', 4], ['Vladimir Petrov', 6],
                                      ['Vita Mironova', 61], ['Andrey Lomov', 10],
                                      ['Andrey Lomovoy', 10], ['Sergey Orlov', 9],
                                      ['Alex Pushkin', 7], ['Viktor Komov', 5],
                                      ['Yan Petrov', 3], ['Platon Kuskov', 2]]
        self.test_obj.order = 'name'  # ключ: сортировка по имени
        self.test_obj.plotting_graph()
        sleep(1)

        self.test_obj.order = 'activ'  # ключ: сортировка по активности
        self.test_obj.plotting_graph()

if __name__ == '__main__':
    unittest.main()
