#!/usr/bin/env python3
"""
git-repo
Программа,выводящая активность по опредёлённому проекту
или список проектов пользователей gitlab,
данные по которым находятся в заданном файле,
по следующим критериям на выбор:
    1. самые большие
    2. больше заданного размера
    3. содержащие в members кого-то,кроме создателя и преподавателя
"""
import sys

import click
import gitlab

from git_repo_size import authorization, get_usernames_list, get_projects_id

GIT_URL = 'https://gitlab.com'
ACCESS_LEVELS = {10, 20, 30, 40}


# pylint: disable=too-many-instance-attributes
class GitRepoMembers:
    """
    Attributes:
        add_list: список members, добавляемых в проекты
        top_table: список [[ссылка на проект, мн-во лишних участников,
        объект project Gitlab API, соответсутвющий проекту],...]
        connect: объект соединения с сервером gitwork.ru
    """
    connect = gitlab.Gitlab(GIT_URL)
    add_list = list()
    del_list = list()
    top_table = list()

    # pylint: disable=too-many-arguments
    def __init__(self, path, members, token="", add=("", 10), delete="", subject=""):
        """
        Args:
            path: Путь до файла с данными пользователей,
                  проекты которых могут иметь лишние members
            token: Приватный токен администратора
            members: путь до файла с данными пользователей,
                     относительно которых определяются лишние members
            add: путь до файла с данными пользователей,
                 добавляемых в members найденным проектам;
                 уровень доступа access_level (/path;access_lvl)
            delete: путь до файла с данными удаляемых пользователей
            subject: название дициплины для поиска
        """
        self.usernames = get_usernames_list(path)
        self.prepods = get_usernames_list(members)
        self.token = token
        self.add = add[0]
        self.access_lvl = add[1]
        self.delete = delete
        self.subject = subject

    def add_members(self):
        """Осуществляет добавление members в project

        Raises:
            ValueError: передан неверный access_level
            IndexError: в данных пользователя указан неверный username
            gitlab.exceptions.GitlabCreateError: пользователь уже есть в members
        """
        for number in self.top_table:
            project = self.connect.projects.get(number[2])
            try:
                if int(self.access_lvl) not in ACCESS_LEVELS:
                    raise ValueError
            except ValueError:
                sys.exit(f'Задан неверный access_level: {self.access_lvl}')
            for member in self.add_list:
                try:
                    id_ = self.connect.users.list(username=member)[0].id
                    project.members.create({'user_id': id_,
                                            'access_level': self.access_lvl})
                except IndexError:
                    sys.exit(f"Error: Неверный username в списке добавляемых members: {member}")
                except gitlab.exceptions.GitlabCreateError as err:
                    if "Member already exists" in str(err):
                        project.members.delete(id_)
                        project.members.create({'user_id': id_,
                                                'access_level': self.access_lvl})
                    else:
                        sys.exit(f'gitlab.exceptions.GitlabCreateError: {err}')

    def del_members(self):
        """Осуществляет удаление members из project

            Raises:
                IndexError: в данных пользователя указан неверный username
                gitlab.exceptions.GitlabCreateError: пользователя нет в members
            """
        for (count, number)in enumerate(self.top_table):
            project = self.connect.projects.get(number[2])
            for member in self.del_list:
                try:
                    id_ = self.connect.users.list(username=member)[0].id
                    project.members.delete(id_)
                    self.top_table[count][1].remove(member)
                except IndexError:
                    sys.exit(f"IndexError: Неверный username в списке удаляемых members: {member}")
                except gitlab.exceptions.GitlabDeleteError:
                    print(f"Неверное имя пользователя: {member} либо его нет в проекте {number[0]}")

    def top_table_print(self):
        """Функция выводит на экран список проектов в которых есть
        лишние members"""
        print("\n№{:>50}\t{:>15}".format("Ссылка на проект", "Лишние members"))
        for (number, member) in enumerate(self.top_table):
            print("{}{:>50}\t{}".format(number + 1, member[0], member[1]))

    def git_members_worker(self):
        """Функция осуществляющая поиск лишних members в проектах пользователей
            и добавляющая в список top_table в виде:
            [[ссылка, лишние участники, объект project Gitlab API, соответсутвющий проекту],...]"""
        for username in self.usernames:

            projects_id = get_projects_id(self.connect, username)
            for pro_id in projects_id:
                project = self.connect.projects.get(pro_id)
                members = [member.username for member in project.members.list()]
                self.prepods.append(username)
                if set(members) - set(self.prepods):
                    if not self.subject:
                        self.top_table.append([project.web_url,
                                               set(members) - set(self.prepods), pro_id])
                    elif self.subject and project.web_url.split('/').count(self.subject):
                        self.top_table.append([project.web_url,
                                               set(members) - set(self.prepods), pro_id])
                self.prepods.remove(username)
                members.clear()
        self.prepods.clear()

    def git_repo_roles(self):
        """Функция осуществляющая поиск линих members в проектах пользователей
        и добавляющая или удаляющая members проектов, если заданы соответствующие флаги"""
        self.connect = authorization(self.token)

        if self.add:
            self.add_list = get_usernames_list(self.add)
        if self.delete:
            self.del_list = get_usernames_list(self.delete)

        self.git_members_worker()

        if __name__ == "__main__":
            if self.add:
                self.add_members()
            if self.delete:
                self.del_members()


# pylint: disable=too-many-arguments
@click.command()
@click.option('-p', '--path', type=str, required=True)
@click.option('-t', '--token', type=str, required=True)
@click.option('-m', '--members', type=str, required=True)
@click.option('-a', '--add', type=(str, int), default=("", 0))
@click.option('-d', '--delete', type=str, default="")
@click.option('-s', '--subject', type=str, default="")
def repo_roles(path, members, token, add, delete, subject):
    """Программа, выводящая список проектов пользователей gitwork,
        в которых содержатся лишние members

        Args:
            path: Путь до файла с данными пользователей,
                  проекты которых могут иметь лишние members
            token: Приватный токен администратора
            members: путь до файла с данными пользователей,
                     относительно которых определяются лишние members
            add: путь до файла с данными пользователей,
                 добавляемых в members найденным проектам;
                 уровень доступа access_level
            delete: путь до файла с данными пользователей,
                    удаляемых из members найденных проектов
            subject: название дициплины для поиска
    """
    repo_roles_object = GitRepoMembers(path, members, token, add, delete, subject)
    repo_roles_object.git_repo_roles()
    repo_roles_object.top_table_print()


# pylint: disable=no-value-for-parameter
if __name__ == "__main__":
    repo_roles()
