"""Программное средство для запуска в браузере
программ git_repo_size, git_repo_roles"""
import sys
import pickle

from flask import Flask, request, render_template

import git_repo_roles
import git_repo_size


def load_top_table(data, filename):
    """Функция выгрузки данных top_table для использования
    в следующем request запросе"""
    try:
        with open(filename, "wb") as file:
            pickle.dump(data, file)
    except FileNotFoundError as err:
        sys.exit(f'Входной файл не найден в системе. {err}')


def unload_top_table(filename):
    """Функция загрузки данных в top_table для
    использования в текущем request запросе"""
    try:
        with open(filename, "rb") as file:
            _list = pickle.load(file)
    except FileNotFoundError as err:
        sys.exit(f'Входной файл не найден в системе. {err}')
    return _list


def get_token():
    """Считывание токена доступа из файла data/token"""
    try:
        with open("data/token.txt", encoding='utf-8', mode="r") as file:
            token = file.readline().rstrip()
    except UnicodeDecodeError as err:
        sys.exit(f'Недопустимый формат входного файла. {err}')
    except FileNotFoundError as err:
        sys.exit(f'Входной файл не найден в системе. {err}')
    return token


app = Flask(__name__)


@app.route("/", methods=["POST", "GET"])
def home_repo():
    """Запуск домашней страницы веб-приложения"""
    return render_template("home.html")


@app.route("/repo_size", methods=["POST", "GET"])
def flask_repo_size():
    """Запуск страницы для программы git_repo_size веб-приложения"""
    repo_object = git_repo_size.GitRepoSize("data/slush_list")
    repo_object.token = get_token()
    repo_object.top_table.clear()

    if request.method == "POST" and request.form['max']:
        repo_object.max_ = float(request.form['max'])

    if request.method == "POST" and request.form['count']:
        repo_object.numbers = int(request.form['count'])

    if request.method == "POST" and request.form.get('ignore'):
        repo_object.ignore = "data/ignored_list"

    if request.method == "POST" and request.form.get('issue'):
        repo_object.msg = "data/message"

    data_to_send = []
    if request.method == "POST":
        repo_object.git_repo_size()
        data_to_send = repo_object.top_table

    if request.method == "POST" and request.form.get('issue'):
        repo_object.top_table = unload_top_table("data/size_table")
        if repo_object.top_table:
            repo_object.submit_issue()

    if request.method == "POST":
        load_top_table(data_to_send, "data/size_table")

    return render_template("repo_size.html", top_table=data_to_send)


@app.route("/repo_members", methods=["POST", "GET"])
def flask_repo_roles():
    """Запуск страницы для программы git_repo_roles веб-приложения"""
    repo_object = git_repo_roles.GitRepoMembers("data/slush_list", "data/members.txt")
    repo_object.token = get_token()
    repo_object.top_table.clear()

    if request.method == "POST" and request.form['subject']:
        repo_object.subject = request.form['subject']

    if request.method == "POST" and request.form.get('add'):
        repo_object.add = "data/add.txt"

    if request.method == "POST" and request.form["access"]:
        repo_object.access_lvl = int(request.form["access"])

    if request.method == "POST" and request.form.get('del'):
        repo_object.delete = "data/delete.txt"

    data_to_send = []
    if request.method == "POST":
        repo_object.git_repo_roles()
        data_to_send = repo_object.top_table

    refresher_text = ""
    proof_flag = True
    if request.method == "POST" and request.form.get('add'):
        repo_object.top_table = unload_top_table("data/roles_table")
        if repo_object.top_table:
            proof_flag = False
            repo_object.add_members()
            refresher_text = "Нажмите продолжить для обновления данных"

    if request.method == "POST" and request.form.get('del'):
        if proof_flag:
            repo_object.top_table = unload_top_table("data/roles_table")
        if repo_object.top_table:
            repo_object.del_members()
            refresher_text = "Нажмите продолжить для обновления данных"

    if request.method == "POST":
        load_top_table(data_to_send, "data/roles_table")

    return render_template("repo_roles.html", top_table=data_to_send, refresher_text=refresher_text)


if __name__ == '__main__':
    app.run(debug=True)
    with open("data/size_table", "wb") as file_size:
        file_size.seek(0)
    with open("data/roles_table", "wb") as file_roles:
        file_roles.seek(0)
