#!/usr/bin/env python3
"""
git-repo
Программа,выводящая активность по опредёлённому проекту
или список проектов пользователей gitlab,
данные по которым находятся в заданном файле,
по следующим критериям на выбор:
    1. самые большие
    2. больше заданного размера
    3. содержащие в members кого-то,кроме создателя и преподавателя
"""
import sys

import click
import gitlab

from read_file import get_usernames_list

GIT_URL = 'https://gitlab.com'
UNIT_KB = 1024  # 1 килобайт
UNIT_MB = 1024 ** 2  # 1 мегабайт


def authorization(token):
    """Производит авторизацию по токену и возвращает
    объект соединения с сервером gitlab.com

    Args:
        token: приватный токен пользователя
    Returns:
        git: объект соединения с сервером gitlab.com
    """
    try:
        git = gitlab.Gitlab(GIT_URL, private_token=token)
        git.auth()
    except gitlab.exceptions.GitlabAuthenticationError as err:
        sys.exit(f'Error: Ошибка авторизации {err}. Проверьте правильность токена.')
    return git


def get_projects_id(connect, username):
    """Возвращает id проектов пользователя username

    Args:
        username: имя пользователя
        connect: объект соединения с сервером gitlab.com
    Returns:
        projects_id: список id проектов пользователя username
    """
    try:
        user_data = connect.users.list(username=username)[0]
    except IndexError:
        sys.exit(f"Error: Неверный username в списке пользователей: {username}")
    projects_id = [project.id for project in user_data.projects.list()]
    return projects_id


# pylint: disable=too-many-instance-attributes
class GitRepoSize:
    """
    Attributes:
        ignored_projects: множество ссылок на игнорируемые проекты
        top_table: список списков вида [[размер,ссылка проекта, id проекта],...]
        message: сообщения для отправки в issue в виде списока [заголовок, описание]
        connect: объект соединения с сервером gitlab.com
    """
    top_table = list()
    ignored_projects = set()
    message = list()
    connect = gitlab.Gitlab(GIT_URL)

    # pylint: disable=too-many-arguments
    def __init__(self, path, token="", numbers=100, max_=0, ignore="", msg=""):
        self.usernames = get_usernames_list(path)
        self.numbers = numbers
        self.max_ = max_
        self.ignore = ignore
        self.msg = msg
        self.token = token
    # pylint: enable=too-many-arguments

    def ignore_list_function(self):
        """Функция чтения из файла игнорируемых при поиске проектов.
        Каждый проект в файле распологается на новой строке.

        Raises:
            UnicodeDecodeError: Файл другого формата.
            FileNotFoundError: Не получилось найти нужный файл в системе
        """
        try:
            with open(self.ignore, encoding='utf-8', mode="r") as file:
                self.ignored_projects = [line.rstrip() for line in file]
            self.ignored_projects = set(self.ignored_projects)
        except UnicodeDecodeError as err:
            sys.exit(f'Недопустимый формат входного файла. {err}')
        except FileNotFoundError as err:
            sys.exit(f'Входной файл не найден в системе. {err}')

    def issue_message_function(self):
        """Функция чтения из файла сообщения для отправки в issue.
        Сообщение в файле по шаблону: заголовок;описание

        Raises:
            UnicodeDecodeError: Файл другого формата.
            FileNotFoundError: Не получилось найти нужный файл в системе
        """
        try:
            with open(self.msg, encoding='utf-8', mode="r") as file:
                message = file.read()
            self.message = message.split(';')
        except UnicodeDecodeError as err:
            sys.exit(f'Недопустимый формат входного файла. {err}')
        except FileNotFoundError as err:
            sys.exit(f'Входной файл не найден в системе. {err}')

    def top_table_sort(self):
        """Функция сортирует полученные проекты по размеру
        в порядке убывания"""
        self.top_table.sort(key=lambda i: i[0], reverse=True)
        for project in self.top_table:
            size = project[0]
            if size >= UNIT_MB:
                project[0] = "{:.3f}MB".format(size / UNIT_MB)
            elif size >= UNIT_KB:
                project[0] = "{:.3f}KB".format(size / UNIT_KB)
            else:
                project[0] = f'{size}B'

    def top_table_print(self):
        """Функция выводит на экран топ-таблицу больших проектов
        в порядке убывания размера в виде:
        |топ-номер|размер|ссылка проекта|"""
        print("\n№{:>16}\t{}".format("Размер", "Ссылка на проект"))
        for (top_number, num) in enumerate(self.top_table):
            print("{}.{size:>15}\t{ref}".format(top_number + 1, size=num[0], ref=num[1]))
            if top_number + 1 == abs(self.numbers):
                break
        print()

    def select_repo(self):
        """Функция осуществляет отбор топ-таблицы больших проектов по максимальному размеру,
        отсеивает игнорируемые проекты и создаёт issue в отобранных проектах
        """
        connect = self.connect
        for username in self.usernames:

            for project_id in get_projects_id(self.connect, username):
                size = connect.projects.get(project_id, statistics=1).statistics["repository_size"]
                project_ref = connect.projects.get(project_id).web_url
                if project_ref in self.ignored_projects:
                    continue
                if size >= abs(self.max_) * UNIT_MB:
                    self.top_table.append([size, project_ref, project_id])

    def submit_issue(self):
        """Отправка issue в найденные репозитории"""
        connect = self.connect
        for target in self.top_table:
            project_id = target[2]
            print(target[2])
            print(self.connect)
            issue = connect.projects.get(project_id).issues.create({
                'title': self.message[0],
                'description': self.message[1]
            })
            # except gitlab.exceptions.GitlabHttpError as err:
            #     sys.exit(f"{err}: {target[0]} not found")
            # except gitlab.exceptions.GitlabGetError as err:
            #     sys.exit(f"{err}: {target[0]} not found")
            username = connect.projects.get(project_id).owner["username"]
            issue.assignee_id = connect.users.list(username=username)[0].id
            issue.save()

    def git_repo_size(self):
        """Программа, выводящая список больших проектов пользователей gitlab,
        данные по которым находятся в заданном файле
        по шаблону fio;name;username"""
        self.connect = authorization(self.token)
        if self.ignore:
            self.ignore_list_function()

        if self.msg:
            self.issue_message_function()

        self.select_repo()
        self.ignored_projects.clear()

        self.top_table_sort()

        if self.numbers <= len(self.top_table):
            del self.top_table[self.numbers:]

        if __name__ == '__main__':
            if self.message:
                self.submit_issue()
        return 0


# pylint: disable=too-many-arguments
@click.command()
@click.option('-n', '--numbers', type=int, default=100)
@click.option('--max', 'max_', type=float, default=0)
@click.option('-p', '--path', type=str, required=True)
@click.option('-t', '--token', type=str, required=True)
@click.option('-i', '--ignore', type=str, default="")
@click.option('--msg', type=str, default="")
def repo_size(path, token, numbers, max_, ignore, msg):
    """Программа, выводящая список больших проектов пользователей gitlab,
    данные по которым находятся в заданном файле
    по шаблону fio;name;username

    Args:
        path: Путь до файла с данными пользователей
        token: Приватный токен администратора
        numbers: Количество выводимых в топ-список проектов
        max_: Значение размера(в мегабайтах),
             при котором выводится список проектов,
             превышающих его
        ignore: Путь до файла со стоп-проектами
        msg: Путь до файла с сообщением по шаблону заголовок;описание,
            отправляемым в issue найденному списку пользователей
    """
    git_repo_object = GitRepoSize(path, token, numbers, max_, ignore, msg)
    git_repo_object.git_repo_size()
    git_repo_object.top_table_print()


# pylint: disable=no-value-for-parameter
if __name__ == '__main__':
    repo_size()
