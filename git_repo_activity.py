"""Модуль определения активности пользоватлей gitlab"""
import sys
from datetime import timedelta, date, datetime

import click
from bokeh.plotting import figure, save

from git_repo_size import authorization

FALL_SEM = '01-09'  # начало осеннего семестра
SPRING_SEM = '01-01'  # начало весеннего семестра


def get_list(path):
    """ Функция, читающая из файла словарь вида username : fullname.
    Шаблон хранимого в файле построчно списка - fio;name;username

    Args:
        path: путь до файла со свписком пользователей
    Returns:
        users_list: Словарь из элементов username : fullname
    Raises:
        UnicodeDecodeError: Файл другого формата
        FileNotFoundError: Не получилось найти нужный файл в системе
    """
    usernames = list()
    fullnames = list()
    try:
        with open(path, encoding='utf-8', mode="r") as file:
            for line in file:
                fullnames.append(line.split(';')[0].rstrip())
                usernames.append(line.split(';')[2].rstrip())
        users_list = dict(zip(usernames, fullnames))
    except UnicodeDecodeError as err:
        sys.exit(f'Недопустимый формат входного файла. {err}')
    except FileNotFoundError as err:
        sys.exit(f'Входной файл не найден в системе. {err}')
    return users_list


# pylint: disable=too-many-instance-attributes
class GitRepoActivity:
    """Класс для определения активности пользователей в проекте"""
    connect = str()
    users_dict = dict()
    username_list = list()
    time_frame = list()
    graphic_data = list()

    # pylint: disable=too-many-arguments
    def __init__(self, path='', token='', frame='', subject='', order="activ"):
        self.path = path
        self.token = token
        self.frame = frame
        self.subject = subject
        self.order = order

    def time_frame_function(self):
        """Определение временных рамок"""
        today = date.today()
        dt_day = timedelta(days=1)
        dt_week = timedelta(days=7)
        dt_month = timedelta(days=30)
        this_year = today.strftime("%Y")
        this_month = today.strftime("%m")
        if self.frame in 'semester':
            if int(this_month) < 9:
                self.time_frame.append(f"{SPRING_SEM}-{this_year}")
            else:
                self.time_frame.append(f"{FALL_SEM}-{this_year}")
        else:
            if self.frame in 'month':
                begin_frame = today - dt_month
            elif self.frame in 'week':
                begin_frame = today - dt_week
            else:
                begin_frame = today - dt_day
            self.time_frame.append(begin_frame.strftime("%d-%m-%Y"))
        end_frame = today + dt_day
        self.time_frame.append(end_frame.strftime("%d-%m-%Y"))

    def commits_calculation(self, events, username):
        """Функция, вычисляющая количество коммитов пользователя в проекте"""
        commit_count = 0
        for event in events:
            if event.author_username == username and event.push_data["action"] == "pushed":
                commit_count += event.push_data["commit_count"]
        self.graphic_data.append([self.users_dict[username], commit_count])

    def select_graphic_data(self, username):
        """Функция, отбирающая коммиты по определённой дисциплине
        и промежутку времени"""
        connect = self.connect
        user = connect.users.list(username=username)[0]
        for project in user.projects.list():
            if project.name == self.subject:
                project = connect.projects.get(project.id)
                events = project.events.list(after=self.time_frame[0],
                                             before=self.time_frame[1],
                                             action="pushed")
                self.commits_calculation(events, username)
                break

    def repo_activity_function(self):
        """Функция, выводящая график активности списка пользователей
        за определённый промежуток времени(семестр, месяц, неделя, день)"""
        self.users_dict = get_list(self.path)
        self.username_list = self.users_dict.keys()
        self.connect = authorization(self.token)

        self.time_frame_function()
        for username in self.username_list:
            self.select_graphic_data(username)

        self.plotting_graph()

    def sort_data(self):
        """Сортировка выводимых данных
        по активности или имени пользователей"""
        if self.order in 'activ':
            self.graphic_data.sort(key=lambda i: i[1])
        elif self.order in 'name':
            self.graphic_data.sort(key=lambda i: i[0])

    def plotting_graph(self):
        """Функция построения графика активности"""
        self.sort_data()
        users = [data[0] for data in self.graphic_data]
        commit_count = [data[1] for data in self.graphic_data]

        graph = figure(y_range=users, plot_width=1000, sizing_mode="scale_height",
                       tooltips=[('Пользователь', '@y'), ('Коммитов', '@right')])
        graph.toolbar_location = None
        graph.title.text_font_size = '20px'
        graph.title.text_font = 'moon flower'
        graph.title.text_font_style = 'italic'
        graph.yaxis.axis_label = 'Пользователи'
        graph.xaxis.axis_label = 'Число коммитов'
        graph.axis.axis_label_text_font = 'italic'
        graph.axis.axis_label_text_font_size = '17px'
        graph.axis.major_label_text_font_size = '13px'
        graph.title.text = f"Активность по дисциплине '{self.subject}' " \
                           f"за период {self.time_frame[0]} : {self.time_frame[1]}"

        # pylint: disable=too-many-function-args
        graph.hbar(users,
                   left=0,
                   right=commit_count,
                   height=.9,
                   fill_color='azure',
                   line_color='green',
                   line_alpha=.5)

        now = datetime.now().strftime('%d-%m-%Y-%X')
        save(graph, f"activ{now}.html")


@click.command()
@click.option('-p', '--path', type=str, required=True)
@click.option('-t', '--token', type=str, required=True)
@click.option('-f', '--frame', required=True,
              type=click.Choice(['sem', 'mon', 'week', 'day']))
@click.option('-s', '--subject', type=str, required=True)
@click.option('-o', '--order', default='activ',
              type=click.Choice(['name', 'activ']))
def git_repo_activity(path, token, frame, subject, order):
    """Программа, выводящая график активности списка пользователей
    за определённый промежуток времени(семестр, месяц, неделя, день)

    Args:
        path: Путь до файла с данными пользователей
        token: Приватный токен администратора
        frame: временные рамки для просмотра активности пользователей(sem, mon, week, day)
        subject: учебная дисциплина для анализа
        order: порядок вывода пользователей( по имени(name), по активности(activ) )

    """
    activity_object = GitRepoActivity(path, token, frame, subject, order)
    activity_object.repo_activity_function()


# pylint: disable=no-value-for-parameter
if __name__ == '__main__':
    git_repo_activity()
